package soulseek

// String represents a string object
type String struct {
	Length  uint32
	Content []byte
}
