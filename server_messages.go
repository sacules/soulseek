package soulseek

// Codes to send to the server
const (
	CodeLogin                       uint32 = 1
	CodeSetListenPort               uint32 = 2
	CodeGetPeerAdrr                 uint32 = 3
	CodeAddUser                     uint32 = 5
	CodeUnknown                     uint32 = 6
	CodeGetStatus                   uint32 = 7
	CodeSayInChatRoom               uint32 = 13
	CodeJoinRoom                    uint32 = 14
	CodeLeaveRoom                   uint32 = 15
	CodeUserJoinedRoom              uint32 = 16
	CodeUserLeftRoom                uint32 = 17
	CodeConnectToPeer               uint32 = 18
	CodePrivateMessages             uint32 = 22
	CodeAcknowledgePrivateMessage   uint32 = 23
	CodeFileSearch                  uint32 = 26
	CodeSetOnlineStatus             uint32 = 28
	CodePing                        uint32 = 32
	CodeSendSpeed                   uint32 = 34
	CodeSharedFoldersAndFiles       uint32 = 35
	CodeGetUserStats                uint32 = 36
	CodeQueuedDownloads             uint32 = 40
	CodeKickedFromServer            uint32 = 41
	CodeUserSearch                  uint32 = 42
	CodeInterestAdd                 uint32 = 51
	CodeInterestRemove              uint32 = 52
	CodeGetRecommendations          uint32 = 54
	CodeGetGlobalRecommendations    uint32 = 56
	CodeGetUserInterests            uint32 = 57
	CodeRoomList                    uint32 = 64
	CodeExactFileSearch             uint32 = 65
	CodeGlobalAdminMessage          uint32 = 66
	CodePrivilegedUsers             uint32 = 69
	CodeHaveNoParents               uint32 = 71
	CodeParentIP                    uint32 = 73
	CodeParentMinSpeed              uint32 = 83
	CodeParentSpeedRatio            uint32 = 84
	CodeParentInactivityTimeout     uint32 = 86
	CodeSearchInactivityTimeout     uint32 = 87
	CodeMinimumParentsInCache       uint32 = 88
	CodeDistributedAliveInterval    uint32 = 90
	CodeAddPrivilegedUser           uint32 = 91
	CodeCheckPrivileges             uint32 = 92
	CodeSearchRequest               uint32 = 93
	CodeAcceptChildren              uint32 = 100
	CodeNetInfo                     uint32 = 102
	CodeWishlistSearch              uint32 = 103
	CodeWishlistInterval            uint32 = 104
	CodeGetSimilarUsers             uint32 = 110
	CodeGetItemRecommendations      uint32 = 111
	CodeGetItemSimilarUsers         uint32 = 112
	CodeRoomTickers                 uint32 = 113
	CodeRoomTickerAdd               uint32 = 114
	CodeRoomTickerRemove            uint32 = 115
	CodeSetRoomTicker               uint32 = 116
	CodeHatedInterestAdd            uint32 = 117
	CodeHatedInterestRemove         uint32 = 118
	CodeRoomSearch                  uint32 = 120
	CodeSendUploadSpeed             uint32 = 121
	CodeUserPrivileges              uint32 = 122
	CodeGivePrivileges              uint32 = 123
	CodeNotifyPrivileges            uint32 = 124
	CodeAcknowledgeNotifyPrivileges uint32 = 125
	CodeBranchLevel                 uint32 = 126
	CodeBranchRoot                  uint32 = 127
	CodeChildDepth                  uint32 = 129
	CodePrivateRoomUsers            uint32 = 133
	CodePrivateRoomAddUser          uint32 = 134
	CodePrivateRoomRemoveUser       uint32 = 135
	CodePrivateRoomDropMembership   uint32 = 136
	CodePrivateRoomDropOwnership    uint32 = 137
	CodePrivateRoomUnknown          uint32 = 138
	CodePrivateRoomAdded            uint32 = 139
	CodePrivateRoomRemoved          uint32 = 140
	CodePrivateRoomToggle           uint32 = 141
	CodePrivateRoomAddOperator      uint32 = 143
	CodePrivateRoomRemoveOperator   uint32 = 144
	CodePrivateRoomOperatorAdded    uint32 = 145
	CodePrivateRoomOperatorRemoved  uint32 = 146
	CodePrivateRoomOwned            uint32 = 148
	CodeCannotConnect               uint32 = 1001
)

// ServerMessage represents the messages sent by a client to interface
// with the server
type ServerMessage struct {
	Length   uint32
	Code     uint32
	Contents []byte
}

// LoginSend is the message used to send a username, password, and
// client version
type LoginSend struct {
	Username String

	// A non-empty string is required
	Password String

	// Version number - 182 for Museek+ 181 for Nicotine+
	Version uint32

	// MD5 hex digest of concatenated username & password
	PassHash String

	// 1 ??? No idea what exactly this is.
	Number uint32

	// In 157 and up
	Unknown String
}

// LoginReceiveSuccess is the message returned from the server if the login
// was successful
type LoginReceiveSuccess struct {
	// A byte representing 1 as a success
	Success byte

	// A MOTD string
	Greet String

	// Your IP address
	IP uint32

	// MD5 hex digest of the password string. Windows Soulseek uses this hash
	// to determine if it's connected to the official server
	PassDigest String
}

// LoginReceiveFailure is the message returned by the server if the login
// failed
type LoginReceiveFailure struct {
	// A byte representing 0 as a failure
	Failure byte

	// Almost always: Bad Password; sometimes it's a banned message
	// or another error.
	Reason String
}

// ListenPort is the port you listen for connections on (2234 by default)
type ListenPort uint32

// PeerAddressSend is a server request for a user's IP Address and port
type PeerAddressSend struct {
	Username String
}

// PeerAddressReceive is a server response for a user's IP Address and port
type PeerAddressReceive struct {
	Username String
	IP       uint32
	Port     uint32
}

// AddUser watches this user's status
type AddUser struct {
	Username String
	Exists   bool

	// 0: Offline
	// 1: Away
	// 2: Online
	Status uint32

	AvgSpeed    uint32
	DownloadNum int64
	Files       uint32
	Dirs        uint32
	CountryCode String
}
